package com.alex.account.billing.service;

public class ConcurrentAccountOperationsException extends RuntimeException {

    public ConcurrentAccountOperationsException(String message) {
        super(message);
    }

    public ConcurrentAccountOperationsException(String message, Throwable cause) {
        super(message, cause);
    }
}
