package com.alex.account.billing.service;

import com.alex.account.billing.domain.Account;

import java.math.BigDecimal;
import java.util.List;

public interface AccountBillingService {
    List<Account> getUserBillingAccountsByUserName(String userName);

    Account addMoneyToAccount(Long accountId, String userName, BigDecimal amount);

    Account getMoneyFromAccount(Long accountId, String userName, BigDecimal amount);

    Account transferMoneyToAccount(Long sourceAccountId, String userName, Long targetAccountId, BigDecimal amount);

    Account getUserBillingAccount(Long accountId, String userName);

    Account createAccount(String userName);
}
