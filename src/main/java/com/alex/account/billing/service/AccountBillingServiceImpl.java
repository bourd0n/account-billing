package com.alex.account.billing.service;

import com.alex.account.billing.domain.Account;
import com.alex.account.billing.repository.AccountRepository;
import com.alex.account.billing.security.domain.User;
import com.alex.account.billing.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
public class AccountBillingServiceImpl implements AccountBillingService {

    private static final String CONCURRENT_ACCOUNT_OPERATIONS = "Some concurrent operations with account with ID %s are performed. Please retry.";
    private static final String CONCURRENT_ACCOUNTS_OPERATIONS = "Some concurrent operations with accounts with ID %s and ID %s are performed. Please retry.";
    private static final String USERNAME_NOT_EXISTS = "The username %s doesn't exist";

    private final AccountRepository accountRepository;

    private final UserRepository userRepository;

    @Autowired
    public AccountBillingServiceImpl(AccountRepository accountRepository, UserRepository userRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Account> getUserBillingAccountsByUserName(String userName) {
        User user = userRepository.findByUsername(userName)
                .orElseThrow(() -> new UserNotFoundException(String.format(AccountBillingServiceImpl.USERNAME_NOT_EXISTS, userName)));
        return accountRepository.findAccountsByUserId(user.getId());
    }

    @Override
    @Transactional
    public Account createAccount(String userName) {
        User user = userRepository.findByUsername(userName)
                .orElseThrow(() -> new UserNotFoundException(String.format(AccountBillingServiceImpl.USERNAME_NOT_EXISTS, userName)));
        Account account = new Account(user);
        return accountRepository.save(account);
    }

    @Override
    public Account getUserBillingAccount(Long accountId, String userName) {
        User user = userRepository.findByUsername(userName)
                .orElseThrow(() -> new UserNotFoundException(String.format(AccountBillingServiceImpl.USERNAME_NOT_EXISTS, userName)));
        Account account = accountRepository.findAccountByIdAndUser(accountId, user)
                .orElseThrow(() -> new AccountNotFoundException(accountId));
        return account;
    }

    @Override
    @Transactional
    public Account addMoneyToAccount(Long accountId, String userName, BigDecimal amount) {
        try {
            Account account = findAccount(userName, accountId);
            account.addMoney(amount);
            return accountRepository.save(account);
        } catch (PessimisticLockingFailureException e) {
            throw new ConcurrentAccountOperationsException(String.format(AccountBillingServiceImpl.CONCURRENT_ACCOUNT_OPERATIONS, accountId), e);
        }
    }

    @Override
    @Transactional
    public Account getMoneyFromAccount(Long accountId, String userName, BigDecimal amount) {
        try {
            Account account = findAccount(userName, accountId);
            return accountRepository.save(account);
        } catch (PessimisticLockingFailureException e) {
            throw new ConcurrentAccountOperationsException(String.format(AccountBillingServiceImpl.CONCURRENT_ACCOUNT_OPERATIONS, accountId), e);
        }
    }

    @Override
    @Transactional
    public Account transferMoneyToAccount(Long sourceAccountId, String userName, Long targetAccountId, BigDecimal amount) {
        try {
            Account sourceAccount = findAccount(userName, sourceAccountId);
            Account targetAccount = accountRepository.findById(targetAccountId)
                    .orElseThrow(() -> new AccountNotFoundException(targetAccountId));
            sourceAccount.subtractMoney(amount);
            targetAccount.addMoney(amount);
            return accountRepository.save(sourceAccount);
        } catch (PessimisticLockingFailureException e) {
            throw new ConcurrentAccountOperationsException(String.format(AccountBillingServiceImpl.CONCURRENT_ACCOUNTS_OPERATIONS, sourceAccountId, targetAccountId), e);
        }
    }

    private Account findAccount(String userName, Long accountId) throws UserNotFoundException, AccountNotFoundException {
        User user = userRepository.findByUsername(userName)
                .orElseThrow(() -> new UserNotFoundException(String.format(USERNAME_NOT_EXISTS, userName)));
        Account account = accountRepository.findById(accountId).orElse(null);
        if (account == null || !account.getUser().getId().equals(user.getId())) {
            throw new AccountNotFoundException(accountId);
        }
        return account;
    }
}
