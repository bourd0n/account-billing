package com.alex.account.billing.security.repository;

import com.alex.account.billing.security.domain.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
}
