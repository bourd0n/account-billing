package com.alex.account.billing.domain;

import com.alex.account.billing.security.domain.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "billing_account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JsonIgnore
    private User user;

    @Column(name = "credit")
    private BigDecimal credit;

    protected Account() {
    }

    public Account(User user) {
        this.user = user;
        this.credit = BigDecimal.ZERO;
    }

    public Account(User user, BigDecimal credit) {
        this.user = user;
        this.credit = credit;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void addMoney(BigDecimal amount) {
        this.credit = this.credit.add(amount);
    }

    public void subtractMoney(BigDecimal amount) {
        BigDecimal newCredit = this.credit.subtract(amount);
        if (newCredit.compareTo(BigDecimal.ZERO) < 0) {
            //todo
            throw new IllegalStateException("Account don't have enought money");
        }
        this.credit = newCredit;
    }

    @Override
    public String
    toString() {
        return "Account{" +
                "id=" + id +
                ", user=" + user +
                ", credit=" + credit +
                '}';
    }
}
