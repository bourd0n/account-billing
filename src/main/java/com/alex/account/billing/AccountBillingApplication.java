package com.alex.account.billing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountBillingApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountBillingApplication.class, args);
    }
}
