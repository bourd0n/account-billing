package com.alex.account.billing.web.rest;

import com.alex.account.billing.domain.Account;
import com.alex.account.billing.service.AccountBillingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@PreAuthorize("hasAuthority('STANDARD_USER')")
@RequestMapping("/api")
public class AccountBillingResource {

    private static final Logger log = LoggerFactory.getLogger(AccountBillingResource.class);

    private final AccountBillingService accountBillingService;

    @Autowired
    public AccountBillingResource(AccountBillingService accountBillingService) {
        this.accountBillingService = accountBillingService;
    }

    @GetMapping("/user/accounts")
    public ResponseEntity<List<Account>> currentUserBillingAccounts(Authentication authentication) {
        log.debug("Current user accounts was requested for user {}", authentication.getName());
        List<Account> accounts = accountBillingService.getUserBillingAccountsByUserName(authentication.getName());
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    @GetMapping("/account/{accountId}")
    public ResponseEntity<Account> getUserBillingAccount(@PathVariable Long accountId,
                                                         Authentication authentication) {
        log.debug("Account {} was requested for user {}", accountId, authentication.getName());
        Account account = accountBillingService.getUserBillingAccount(accountId, authentication.getName());
        return new ResponseEntity<>(account, HttpStatus.OK);
    }

    @PostMapping("/account/create")
    @Transactional
    public ResponseEntity<Account> createAccount(Authentication authentication) {
        Account result = accountBillingService.createAccount(authentication.getName());
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @PostMapping("/account/{accountId}/money/add")
    @Transactional
    public ResponseEntity<Account> addMoneyToAccount(@PathVariable Long accountId,
                                                     @RequestParam BigDecimal amount,
                                                     Authentication authentication) {
        Account result = accountBillingService.addMoneyToAccount(accountId, authentication.getName(), amount);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/account/{accountId}/money/get")
    @Transactional
    public ResponseEntity<Account> getMoneyFromAccount(@PathVariable Long accountId,
                                                       @RequestParam BigDecimal amount,
                                                       Authentication authentication) {
        Account result = accountBillingService.getMoneyFromAccount(accountId, authentication.getName(), amount);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/account/{sourceAccountId}/transfer")
    @Transactional
    public ResponseEntity<Account> transferMoneyToAccount(@PathVariable Long sourceAccountId,
                                                          @RequestParam Long targetAccountId,
                                                          @RequestParam BigDecimal amount,
                                                          Authentication authentication) {
        Account result = accountBillingService.transferMoneyToAccount(sourceAccountId, authentication.getName(), targetAccountId, amount);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
