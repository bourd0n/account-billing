package com.alex.account.billing.web.rest;

import com.alex.account.billing.domain.Account;
import com.alex.account.billing.security.domain.User;
import com.alex.account.billing.security.repository.UserRepository;
import com.alex.account.billing.service.AccountBillingService;
import com.alex.account.billing.service.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@PreAuthorize("hasAuthority('ADMIN_USER')")
@RequestMapping("/api/admin")
public class AdminAccountBillingResource {

    private static final Logger log = LoggerFactory.getLogger(AccountBillingResource.class);

    private final AccountBillingService accountBillingService;

    private final UserRepository userRepository;

    @Autowired
    public AdminAccountBillingResource(AccountBillingService accountBillingService, UserRepository userRepository) {
        this.accountBillingService = accountBillingService;
        this.userRepository = userRepository;
    }

    @GetMapping("/user/{userId}/accounts")
    public ResponseEntity<List<Account>> userBillingAccounts(@PathVariable Long userId) {
        log.debug("Current user accounts was requested for userId {}", userId);
        User user = userRepository.findById(userId).orElseThrow(
                () -> new UserNotFoundException(String.format("User with ID %s not found", userId))
        );
        List<Account> accounts = accountBillingService.getUserBillingAccountsByUserName(user.getUsername());
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }
}
