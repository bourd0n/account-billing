package com.alex.account.billing.repository;

import com.alex.account.billing.domain.Account;
import com.alex.account.billing.security.domain.User;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account, Long> {

    List<Account> findAccountsByUserId(Long userId);

    Optional<Account> findAccountByIdAndUser(Long accountId, User user);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<Account> findById(Long accountId);
}
