CREATE TABLE app_role (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  description varchar(255) DEFAULT NULL,
  role_name varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE app_user (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  username varchar(255) NOT NULL UNIQUE ,
  PRIMARY KEY (id)
);


CREATE TABLE user_role (
  user_id bigint(20) NOT NULL,
  role_id bigint(20) NOT NULL,
  CONSTRAINT user_role_app_user_fk FOREIGN KEY (user_id) REFERENCES app_user (id),
  CONSTRAINT user_role_app_role_fk FOREIGN KEY (role_id) REFERENCES app_role (id)
);

CREATE TABLE billing_account (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  user_id bigint(20) NOT NULL,
  credit decimal,
  CONSTRAINT billing_account_app_user_fk FOREIGN KEY (user_id) REFERENCES app_user (id),
  PRIMARY KEY (id)
);

