Security implementation get from [this article](https://medium.com/@nydiarra/secure-a-spring-boot-rest-api-with-json-web-token-reference-to-angular-integration-e57a25806c50)
Generally authentication provider should be separate service, so here is some stub and simple implementation.

Get token:
```
curl jwtclientid:XY7kmzoNzl100@localhost:8080/oauth/token -d grant_type=password -d username=john.doe -d password=jwtpass
```